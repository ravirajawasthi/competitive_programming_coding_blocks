#include <bits/stdc++.h>
#include <string>
using namespace std;

int main(int argc, char const *argv[])
{

#ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif

    string s0;
    string s1("hello");
    string s2 = "hello world";

    string s3(s2);

    string s4 = s3;
    char a[] = {'a', 'b', 'c', '\0'};

    string s5(a);
    cout << s1 << endl;
    cout << s2 << endl;
    cout << s3 << endl;
    cout << s4 << endl;
    cout << s5 << endl;
    return 0;
}
