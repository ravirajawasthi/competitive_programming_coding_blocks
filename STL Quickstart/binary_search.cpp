#include <iostream>
#include <algorithm>
using namespace std;

int main(int argc, char const *argv[])
{
    int arr[] = {1, 2, 3, 40, 40, 50, 100, 110};
    int n = sizeof(arr) / sizeof(int);
    int key;
    cin >> key;

    bool present = binary_search(arr, arr + n, key);

    if (present)
    {
        cout << "Present";
    }

    else
    {
        cout << "Not present";
    }

    cout << endl;

    auto upb = lower_bound(arr, arr + n, 40); // first element greater or equal to 40
    int index = arr - upb;
    cout << "lower bound " << index << endl;

    auto lb = upper_bound(arr, arr + n, 40); // first element strictly greater than 40
    int index2 = arr - lb;
    cout << "upperbound bound " << index2 << endl;

    // Occurence of 40 is
    cout << "Occurrence of 40 = " << upb - lb << endl;

    return 0;
}
