#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const *argv[])
{
    int n, m, i, j;

    cin >> n >> m >> i >> j;
    int mask_i = (1 << i) - 1;  // Getting all the ones {2**n - 1} 0111 so all bits maintain their original value after and operation
    int mask_j = (~0) << j + 1; // Getting all the zeros {2**n - 1} 100000 so all the unwanted bits are cleared
    int mask = mask_i | mask_j; // Only those bits are zero which we need to clear hence after AND operation those bits are cleared

    int a = (n & mask) | (m << i);

    cout << a << endl;

    return a;
}
