#include <bits/stdc++.h>

using namespace std;

int countSetBits(int num)
{
    int ans = 0;
    while (num > 0)
    {
        num = num & (num - 1); // remove the last set bit
        ans++;
    }
    return ans;
}

int main()
{
    int t;
    cin >> t;
    int a, b, ans, start, end;
    int i, j; // Loop variables
    for (i = 0; i < t; i++)
    {
        cin >> a >> b;
        start = min(a, b);
        end = max(a, b);
        ans = 0;
        for (j = start; j <= end; j++)
        {
            if (countSetBits(j) != __builtin_popcount(j))
            {
                throw 404;
            }
            ans += countSetBits(j);
        }
        cout << ans << endl;
    }
    return 0;
}
