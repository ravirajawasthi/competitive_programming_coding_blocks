from math import pow


def longestPalindromicSubsequences(s: str) -> int:
    n = len(s)
    count = n

    max_till_now_len = 1
    max_till_now = s[0]
    dp = [[True if i == j else False for j in range(n)] for i in range(n)]

    for i in range(0, n - 1):
        j = i + 1
        if s[i] == s[j]:
            dp[i][j] = True
            count += 1
            if len(s[i : j + 1]) > max_till_now_len:
                max_till_now_len = len(s[i : j + 1])
                max_till_now = s[i : j + 1]

    for offset in range(2, n):
        j = offset
        for i in range(0, n - offset):
            if s[i] == s[j] and dp[i + 1][j - 1]:
                dp[i][j] = True
                count += 1
                if len(s[i : j + 1]) > max_till_now_len:
                    max_till_now_len = len(s[i : j + 1])
                    max_till_now = s[i : j + 1]

            j += 1

    return max_till_now


s = "fccacc"
print(longestPalindromicSubsequences(s))
