from math import pow
import math


def longestPalindromicSubsequences(s: str) -> int:
    n = len(s)
    count = n

    dp = [[True if i == j else False for j in range(n)] for i in range(n)]

    for i in range(0, n - 1):
        j = i + 1
        if s[i] == s[j]:
            dp[i][j] = True
            count += 1

    for offset in range(2, n):
        j = offset
        for i in range(0, n - offset):
            if s[i] == s[j] and dp[i + 1][j - 1]:
                dp[i][j] = True
                count += 1
            j += 1

    return int(count % (math.pow(10, 7) + 7))


s = "fccacc"
print(longestPalindromicSubsequences(s))
