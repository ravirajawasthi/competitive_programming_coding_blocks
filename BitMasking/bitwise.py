def isOdd(n):
    return f"{n} is odd" if n & 1 == 1 else f"{n} is even"


def getBit(n, i):
    """get ith bit from right"""
    mask = 1 << i
    return "requested bit is 1" if mask & n == 1 else "requested bit is 0"


def setBit(n, i):
    """set ith bit from right"""
    mask = 1 << i
    return f"{n} becomes {n | mask}"


def clearBit(n, i):
    """clear ith bit from tight"""
    mask = ~(
        1 << i
    )  # Only bit that we have to clear is 0 rest are all 1, hence with and operation we will get number with ith bit cleared
    return n & mask


def updateBit(n, i, b):
    """update ith bit to b"""
    # Clear ith bit first
    mask = ~(1 << i)
    n = n & mask

    # set ith bit to b
    mask = b << i
    n = n | mask

    return n


def clearLastIBits(n, i):
    mask = (
        -1 << i
    )  # -1 is all the bits set to "1" [2's compliment way of storing negative number]
    # mask = (~0) << i #Same at above approach

    return n & mask


def clearItoJBits(n, i, j):
    """
    clear i to j bits of n
    """
    if j >= i:
        raise Exception("j cannot be less than or equal to i")

    mask_i = -1 << (i + 1)

    mask_j = (1 << j) - 1

    mask = mask_i | mask_j

    return mask & n


# 13 - 1101
# 14 - 1110
# 10 - 1010

print(isOdd(15))
print(getBit(13, 2))
print(setBit(13, 1))
print(updateBit(14, 0, 1))
print(clearLastIBits(10, 2))
print(clearItoJBits(31, 3, 1))
