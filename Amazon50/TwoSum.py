from collections import defaultdict
from typing import List


def twoSum(nums: List[int], target: int) -> List[int]:
    d = defaultdict(list)
    for ind, el in enumerate(nums):
        d[el].append(ind)

    for prefix in d.keys():
        remaining = target - prefix
        if len(d.get(remaining, [])) == 0:
            continue
        else:
            if remaining == prefix:
                if len(d[remaining]) == 1:
                    continue
                return d.get(prefix)[:1]
            else:
                return [d.get(prefix)[0], d.get(remaining)[0]]


nums = [3, 2, 4]
target = 6

print(twoSum(nums, target))
