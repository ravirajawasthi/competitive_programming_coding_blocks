#include <bits/stdc++.h>
using namespace std;

int lastSetBit(int num)
{
    int pos = 0, mask = 1;
    while ((num & mask) == 0)
    {
        pos++;
        mask = mask << 1;
    }
    return pos;
}

bool isBitSetAtPos(int num, int pos)
{
    int mask = 1 << pos;
    if ((num & mask) != 0)
        return true;
    return false;
}

int main()
{
    int nums, it;
    cin >> nums;
    int all_xor = 0;
    int a, b;
    vector<int> list;

    for (int i = 0; i < nums; i++)
    {
        cin >> it;
        list.push_back(it);
        all_xor = all_xor ^ it;
    }

    int lsb = lastSetBit(all_xor);

    a = 0;

    for (int n : list)
    {
        if (isBitSetAtPos(n, lsb))
        {
            a ^= n;
        }
    }
    b = all_xor ^ a;
    cout << min(a, b) << " " << max(a, b) << endl;
    return 0;
}