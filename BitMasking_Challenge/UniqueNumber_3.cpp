#include <bits/stdc++.h>
using namespace std;
int main()
{
    int n, temp;
    cin >> n;
    int nums[n] = {0};

    for (int i = 0; i < n; i++)
    {
        cin >> temp;
        nums[i] = temp;
    }

    int bitSum[64] = {0};

    for (int i = 0; i < n; i++)
    {
        int num = nums[i];
        int currpos = 0;

        while (num > 0)
        {
            bitSum[currpos] += num & 1;
            num = num >> 1;
            currpos += 1;
        }
    }
    int ans = 0;
    for (int i = 0; i < 64; i++)
    {
        if (bitSum[i] % 3 != 0)
        {
            ans += pow(2, i);
        }
    }
    cout << ans << endl;

    return 0;
}